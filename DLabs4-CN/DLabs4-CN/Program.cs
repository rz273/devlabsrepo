﻿using System;

//Program from the Labs - working with the class

namespace DLabs4
{
    class Program
    {
        //enum example with custom int values
        enum Days
        {
            Mon = 2,
            Tue = 4,
            Wed = 6,
            Thu = 8,
            Fri = 10,
            Sat = 12,
            Sun = 14
        }

        //simple struct
        struct Test
        {
            public int field1;
            public int field2;
        }

        static void Main(string[] args)
        {
            //struct variable declaration
            Test test;

            //struct fields initialization
            test.field1 = 1;
            test.field2 = 1;
            Console.WriteLine(test.field1);
            Console.WriteLine(test.field2);

            //struct initialization with default constructor
            var test2 = new Test();
            Console.WriteLine(test2.field1);
            Console.WriteLine(test2.field2);

            //ComplexNumber tests
            Console.WriteLine(new ComplexNumber(19, 20) + new ComplexNumber(0, -20));
            Console.WriteLine(new ComplexNumber(0, -20));
            Console.WriteLine(new ComplexNumber(0, 0));
            Console.WriteLine(new ComplexNumber(2, -1));

            //DateTime exercises tests
            DateProperties(DateTime.Now);
            DayOfWeek(DateTime.Now);

            Console.ReadKey();
        }

        //Dates exercise solutions
        private static void DateProperties(DateTime day)
        {
            Console.WriteLine($"Year = {day.Year}");
            Console.WriteLine($"Month = {day.Month}");
            Console.WriteLine($"Hours  = {day.Hour}");
            Console.WriteLine($"minute = {day.Minute}");
            Console.WriteLine($"seconds = {day.Second}");
            Console.WriteLine($"ms = {day.Millisecond}");
        }

        //Dates exercise solutions
        private static void DayOfWeek(DateTime someDay)
        {
            Console.WriteLine($"The day of the week for {someDay.ToShortDateString()} is {someDay.DayOfWeek}");
        }
    }
}
