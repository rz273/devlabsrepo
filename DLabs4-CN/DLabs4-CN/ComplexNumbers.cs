﻿namespace DLabs4

//Program from the Labs - working with the class
{
    struct ComplexNumber
    {
        public double Real { get; private set; }
        public double Imaginary { get; private set; }

        public ComplexNumber(double real, double imaginary)
        {
            this.Real = real;
            this.Imaginary = imaginary;
        }
        public override string ToString()
        {
            if (Real == 0)
                return $"{Imaginary}i";

            if (Imaginary < 0)
                return $"{Real}{Imaginary}i";

            return $"{Real}+{Imaginary}i";
        }

        public static ComplexNumber operator +(ComplexNumber c1, ComplexNumber c2)
        {
            ComplexNumber temp = new ComplexNumber(c1.Real + c2.Real, c1.Imaginary + c2.Imaginary);
            return temp;
        }

        public static ComplexNumber operator -(ComplexNumber c1, ComplexNumber c2)
        {
            ComplexNumber temp = new ComplexNumber(c1.Real - c2.Real, c1.Imaginary - c2.Imaginary);
            return temp;
        }
    }
}
