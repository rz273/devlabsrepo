﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLabsC2_2
{
    class Calc
    {
        private static readonly string[] operations = { "1", "2", "3", "4", "5", "6","7","add","substract","divide","square root","square power","exit" };
        static void Main(string[] args)
        {
            //Interactive Calculator
            double result = 0;
            Menu();
            bool workdone=true;
            string option = SetOperation("");
            //var option = Console.ReadLine().Trim();
            double ValueA = SetValues("Type you first number : ");
            double ValueB = SetValues("Type you second number: ");

            while (workdone)
            {
               
                if (option == "exit" || option=="7" )
                {
                    workdone = false;
                    Task.Delay(5);
                }
                switch (option)
                {
                    case "1":
                    case "add":
                        option = "+";
                        result = ValueA + ValueB;
                        workdone = false;
                        break;
                    case "2":
                    case "substract":
                        option = "-";
                        result = ValueA - ValueB;
                        workdone = false;
                        break;
                    case "3":
                    case "multiply":
                        option = "*";
                        result = ValueA * ValueB;
                        workdone = false;
                        break;
                    case "4":
                    case "divide":
                        option = "/";
                        result = ValueA / ValueB;
                        workdone = false;
                        break;
                    case "5":
                    case "square power":
                        option = "^2";
                        result = Math.Pow(ValueA, ValueA);
                        workdone = false;
                        break;
                    case "6":
                    case "square root":
                        option = "√";
                        result = Math.Sqrt(ValueA);
                        workdone = false;
                        break;
                }
            }
            if (option == "√")
                Console.WriteLine("Result of {0}{1} = {2}", ValueA, option, result);
                 if(option=="^2")
                    Console.WriteLine("Result of {0}{1} = {2}", ValueA, option, result);
                 else
                    Console.WriteLine("Result of {0} {1} {2} = {3}", ValueA, option, ValueB, result);
                    Console.ReadKey();


        }
        static void Menu()
        {
            Console.WriteLine("Interactive Calc");
            Console.WriteLine("Choose action");
            Console.WriteLine("1. add");
            Console.WriteLine("2. subtract");
            Console.WriteLine("3. multiply");
            Console.WriteLine("4. divide");
            Console.WriteLine("5. square power");
            Console.WriteLine("6. square root");
            Console.WriteLine("7. exit");
            Console.WriteLine("Choose your option, you can simply write the option like 'add' or just type the number");
        }
        private static double SetValues(string Information)
        {
            double parse;
            Console.Write(Information);
            string tempInput = Console.ReadLine();
            while (!double.TryParse(tempInput, out parse))
            {
                Console.WriteLine("Incorrect input !");
                Console.Write(Information);
                tempInput = Console.ReadLine();
            }
            return double.Parse(tempInput);
        }
        private static bool Valid(string input)
        {
            return operations.Contains(input);
        }

        private static string SetOperation(string outputText)
        {
            Console.Write(outputText);
            string tempInput = Console.ReadLine();
            while (!Valid(tempInput))
            {
                Console.WriteLine("Wrong text, put a number or rewrite correct string !");
                Console.Write(outputText);
                tempInput = Console.ReadLine();
            }
            return tempInput;
        }
    }
    

}