﻿using System;

namespace DLabsC1
{
    class Program
    {
        static void Main(string[] args)
        {
            FoobarDown(15);
            Console.ReadKey();
            foobarUP(15, 1);
            Console.ReadKey();
        }

        private static void NewMethod()
        {
            Console.ReadKey();
        }

        private static void FoobarDown(int n)
        {
            if (n % 3 == 0 && n % 5 == 0)
                Console.WriteLine("FooBar");
            else if (n % 3 == 0)
                Console.WriteLine("Foo");
            else if (n % 5 == 0)
                Console.WriteLine("Bar");
            else
                Console.WriteLine(n);
            n--;
            if (n != 0)
                FoobarDown(n);
        }
        private static void foobarUP(int n, int i)
        {
            if (i % 3 == 0 && i % 5 == 0)
                Console.WriteLine("FooBar");
            else if (i % 3 == 0)
                Console.WriteLine("Foo");
            else if (i % 5 == 0)
                Console.WriteLine("Bar");
            else
                Console.WriteLine(i);
            i++;
            if (i < n + 1)
                foobarUP(n, i);
        }

    }
}
