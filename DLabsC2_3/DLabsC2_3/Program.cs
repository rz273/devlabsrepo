﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLabsC2_3
{
    class Program
    {
        private static List<char> chars;

        static void Main(string[] args)
        {
            Initialize("Jak");
            Append(" ");
            Append("ymienic");
            Append(" jeyzk");
            Append(" klawiaturz?");
            Console.WriteLine(BackToString());

         
            Replace('y', '#');
            Replace('z', '&');
            Replace('#', 'z');
            Replace('&', 'y');

            Console.WriteLine(BackToString());
            Console.ReadKey();
        }

        private static void Initialize(string value)
        {
            chars = new List<char>();
            Append(value);
        }

        private static string BackToString()
        {
            if (chars == null || chars.Count == 0)
                return null;

            var arr = new char[chars.Count];

            for (int i = 0; i < chars.Count; i++)
            {
                arr[i] = chars[i];
            }

            return new string(arr);
        }

        private static void Replace(char oldChar, char newChar)
        {
            if (chars == null || chars.Count == 0)
                return;

            for (int i = 0; i < chars.Count; i++)
            {
                if (chars[i] == oldChar)
                    chars[i] = newChar;
            }
        }

        private static void Append(string value)
        {
            if (chars == null)
                return;

            foreach (char c in value)
                chars.Add(c);
        }
    }
}