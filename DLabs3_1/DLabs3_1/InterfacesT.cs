﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DLabs3_1
{
    interface InterfacesT
        {
            void Insert(int key, object value);
            object Pop();
        }
}
