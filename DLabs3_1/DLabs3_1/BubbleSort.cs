﻿using System;
using System.Collections.Generic;
//CODE FROM teamplace - review
namespace DLabs3_1
{
    static class BubbleBSTSort
    {
        public static void Sort(List<Node> bsts)
        {
            if (bsts == null || bsts.Count == 0)
                return; //nothig to sort

            var len = bsts.Count;

            //indicates if anything was changed during one interation of outer loop
            //if whole iteration of outer loop was without changes, it means in next iteration it will be the same
            //so we can end algorithm
            var anythingChanged = true;

            for (int i = 1; i <= len - 1 && anythingChanged; i++)
            {
                anythingChanged = false;

                for (int j = 0; j < len - 1; j++)
                {
                    object leftMinKeyOb = bsts[j].MinKey();
                    object rightMinKeyOb = bsts[j + 1].MinKey();

                    //we treat null MinKey as Int32.MinValue, because it means tree is empty, so it's smallest
                    //this syntax is shortened if statement
                    //               |       condition      |     if body        |   else body  |
                    var bstMinRight = rightMinKeyOb != null ? (int)rightMinKeyOb : int.MinValue;
                    var bstMinLeft = leftMinKeyOb != null ? (int)leftMinKeyOb : int.MinValue;

                    if (bstMinLeft > bstMinRight)
                    {
                        //procedure of changing two neighbour elements in places
                        var tmp = bsts[j];
                        bsts[j] = bsts[j + 1];
                        bsts[j + 1] = tmp;
                        anythingChanged = true;
                    }
                }

            }
        }
    }
}
