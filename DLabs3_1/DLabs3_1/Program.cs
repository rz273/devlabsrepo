﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
//CODE FROM teamplace - review 
namespace DLabs3_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var root = new Node(8, "val8");
            root.Insert(3, "val3");
            root.Insert(10, "val10");
            root.Insert(1, "val1");
            root.Insert(14, "val14");
            root.Insert(6, "val6");
            root.Insert(13, "val13");
            root.Insert(7, "val7");
            root.Insert(4, "val4");

            Console.WriteLine(root.ToString());

            var bsts = new List<Node>();
            var inceptionRoot = CreateBst(new List<int> { 1, 2 });
            bsts.Add(inceptionRoot);

            var node = CreateBst(new List<int> { 2, 3 });
            inceptionRoot.Insert(2, node);
            bsts.Add(node);
            node = CreateBst(new List<int> { 3, 4 });
            inceptionRoot.Insert(3, node);
            bsts.Add(node);
            node = CreateBst(new List<int> { 1, 8 });
            inceptionRoot.Insert(1, node);
            bsts.Add(node);
            node = CreateBst(new List<int> { 564, 89 });
            inceptionRoot.Insert(89, node);
            bsts.Add(node);
            node = CreateBst(new List<int> { 8, 4 });
            inceptionRoot.Insert(4, node);
            bsts.Add(node);
            node = CreateBst(new List<int> { 8, 6 });
            inceptionRoot.Insert(6, node);
            bsts.Add(node);
            node = CreateBst(new List<int> { 3, 8 });
            inceptionRoot.Insert(3, node);
            bsts.Add(node);
            node = CreateBst(new List<int> { 213, 87 });
            inceptionRoot.Insert(87, node);
            bsts.Add(node);
            node = CreateBst(new List<int> { 7, 1 });
            inceptionRoot.Insert(1, node);
            bsts.Add(node);

            BubbleBSTSort.Sort(bsts);

            foreach (var n in bsts)
                Console.WriteLine($"MinKey: {n.MinKey()}");
            Console.WriteLine(inceptionRoot);

            Console.ReadKey();
        }

        private static Node CreateBst(List<int> keys)
        {
            var valueFormat = "value{0}";

            var nodesCount = keys.Count;

            var rootKey = keys[0];
            var root = new Node(rootKey, string.Format(valueFormat, rootKey));

            for (int i = 1; i < nodesCount; i++)
            {
                var key = keys[i];
                root.Insert(key, string.Format(valueFormat, key));
            }

            return root;
        }
    }
}
