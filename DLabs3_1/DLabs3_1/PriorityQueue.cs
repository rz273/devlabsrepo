﻿using System.Collections.Generic;
// CODE FROM Teamplace - review
namespace DLabs3_1
{
    class PriorityQueue : InterfacesT
    {
        private List<KeyValuePair<int, object>> elements;
        public PriorityQueue()
        {
            elements = new List<KeyValuePair<int, object>>();
        }

        public void Add(int priority, object value)
        {
            var element = new KeyValuePair<int, object>(priority, value);

            if (elements.Count == 0)
            {
                elements.Add(element);
                return;
            }

            var index = 0;
            //search for place to insert new element
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i].Key < element.Key)
                    continue;

                //if we are here it means that we found element of the same or greater value
                //it is not advised to change collection during iterating over it 
                //so it's better to store found index and insert outside loop
                index = i;
            }

            elements.Insert(index, element);
        }

        public object GetAndRemoveTop()
        {
            if (elements.Count == 0)
                return null; //nothing to return

            var toReturn = elements[0];
            elements.RemoveAt(0);
            return toReturn;
        }

        /* IPriorityCollection members */
        public void Insert(int key, object value)
        {
            Add(key, value);
        }

        public object Pop()
        {
            return GetAndRemoveTop();
        }
        /* IPriorityCollection members */
    }
}
