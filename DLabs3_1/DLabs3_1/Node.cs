﻿namespace DLabs3_1
{
    //CODE FROM Teamplace- to review
    //this implementation doesn't have MinKey optimization
    //this otpimization is easy expecting Pop method
    //in Pop you have to find newMinKey and propagate it throug whole tree to update minKeys in all Nodes where Node.Key < newMinKey

    class Node : InterfacesT
    {
        private bool wasPopped;

        public Node(int key, object value)
        {
            Key = key;
            Value = value;
            wasPopped = false;
        }

        //setters are private because we don't want anyone to ruin our structure from outside
        public int Key { get; private set; }

        public object Value { get; private set; }

        public Node Left { get; private set; }
        public Node Right { get; private set; }

        public void Insert(int key, object value)
        {
            //we don't use wasPopped here, because popped nodes still assure "left smaller - right bigger" structure of BST
            //and popped nodes can contain not-popped children

            if (key < this.Key)
            {
                if (Left != null)
                {
                    Left.Insert(key, value);
                }
                else
                {
                    Left = new Node(key, value);
                }
            }
            else
            {
                if (Right != null)
                {
                    Right.Insert(key, value);
                }
                else
                {
                    Right = new Node(key, value);
                }
            }
        }

        public object Pop()
        {
            var minKeyObj = MinKey();

            //this means that in BST there is no minimal key, so it is actyally empty or whole marked as popped
            if (minKeyObj == null)
                return null;

            var minNode = FindNode((int)minKeyObj);

            minNode.wasPopped = true;

            return minNode.Value;
        }

        public object Find(int key)
        {
            var found = FindNode(key);

            if (found != null)
                return found.Value;

            return null;
        }

        private Node FindNode(int key)
        {
            if (this.Key == key && !wasPopped)
                return this;

            if (key < this.Key)
            {
                if (Left == null)
                    return null; //key not present in BST

                return Left.FindNode(key);
            }
            else
            {
                if (Right == null)
                    return null; //key not present in BST

                return Right.FindNode(key);
            }
        }

        public object MinKey()
        {
            object currentMin = null;

            if (Left != null)
                currentMin = Left.MinKey();

            if (currentMin != null)
                return currentMin;

            //we haven't found anything not yet popped in Left or Left was null
            if (!this.wasPopped)
                return this.Key;

            //left and root are not present or already popped
            if (Right != null)
                currentMin = Right.MinKey();

            return currentMin;
        }

        public override string ToString()
        {
            return "{" + this.ToStringInternal() + "}";
        }

        private string ToStringInternal()
        {
            var leftString = string.Empty;
            if (this.Left != null)
                leftString = Left.ToStringInternal() + ", ";

            var rightString = string.Empty;
            if (this.Right != null)
                rightString = ", " + Right.ToStringInternal();

            return leftString + $"[{this.Key}, {this.Value.ToString()}]" + rightString;
        }
    }
}