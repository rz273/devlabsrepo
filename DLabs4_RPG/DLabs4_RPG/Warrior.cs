﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLabs4_RPG
{
    class Warrior : Character
    {

        public Warrior(string name, int health, int strenght) : base(health, strenght, name)
        {
        }

        protected override int CalculateAttackPower()
        {
            if (health >= 5) return strenght * health;
            return strenght * 100;

        }
    }
}