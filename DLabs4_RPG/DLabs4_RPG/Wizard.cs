﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLabs4_RPG
{
    class Wizard : Character
    {

        private int magicPoints;

        public Wizard(string name, int health, int magicPoints, int strenght) : base(health, strenght, name)
        {
            this.magicPoints = magicPoints;
        }

        protected override int CalculateAttackPower()
        {

            return (magicPoints + strenght) * health;
        }
    }
}
