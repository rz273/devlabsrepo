﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLabs4_RPG //F12 + ctrl- routing beetwen base class and the intheritance 
{
    class Program
    {
        static void Main(string[] args)
        {
            var warrior = new Warrior("geralt", 100, 200);

            var team = new Team("druzyna");

            team.Add(warrior);

            var geralt = team["geralt"];

            Console.ReadKey();
        }
    }
}
