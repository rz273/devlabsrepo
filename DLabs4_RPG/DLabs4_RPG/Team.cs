﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLabs4_RPG
{
    class Team
    {
        private List<Character> members = new List<Character>();

        private string teamName;

        public Team(string teamName)
        {
            this.teamName = teamName;
        }

        public void Add(Character memberCharacter)
        {
            members.Add(memberCharacter);
        }

        public Character this[string name]
        {
            get
            {
                if (members == null || members.Count == 0)
                    return null;

                foreach (var n in members)
                {
                    if (n.Name == name)
                        return n;

                }

                return null;
            }
        }
    }
}
