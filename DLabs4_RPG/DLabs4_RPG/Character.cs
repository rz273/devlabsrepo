﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLabs4_RPG
{
    abstract class Character
    {
        protected int health;
        protected int strenght;

        public string Name { get; private set; }

        protected Character(int hp, int strenght, string _name)
        {
            health = hp;
            Name = _name;
            this.strenght = strenght;
        }


        protected abstract int CalculateAttackPower();

        public int Substrakt(int value)
        {
            health -= value;
            if (health > 100)
                health = 100;
            else if (health < 0)
                health = 0;
            return health;
        }

        public override string ToString()
        {
            //return name + ", " + health.ToString() + ", " + CalculateAttackPower();
            //return string.Format("{0}, {1}, {2}", name, health, CalculateAttackPower());
            return $"{Name}, {health}, {CalculateAttackPower()}";
        }
    }
}