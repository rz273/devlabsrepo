﻿using System;
using System.Collections.Generic;
//list_operators
namespace DLabsC2
{
    class Program
    {
        static void Main(string[] args)
        {
            //List implementation for ADD method
            var Lista =  new List<int>();
            var Lista2 = new List<int>();

            //List implementation for ZIP method
            var list1 = new List<object>() { "one", "two", "three" };
            var list2 = new List<object> { 1, 2, 3, 4, 5 };
            
            //Fill List
            for (int i = 0; i < 9; i++)
            {
                //Lista.Add(GetRandomNumber(i, 10));
                //Lista2.Add(GetRandomNumber(i + 1, 10));
                Lista.Add(i);
                int y = i % 2;
                if(y==0)
                Lista2.Add(i + 1);
            }
            PrintTosScreen(Lista);
            Console.WriteLine("     Lista 1");
            PrintTosScreen(Lista2);
            Console.WriteLine("     Lista 2");
            //List_Add Method Implementation
            PrintTosScreen(AddList(Lista, Lista2));
            Console.WriteLine("     Lista wynikowa");
            PrintTosScreen(Zip(list1, list2));
            Console.ReadKey();
        }

        //Pseudo random implementation
        private static readonly Random getrandom = new Random();
        public static int GetRandomNumber(int min, int max)
        {
            lock (getrandom) // synchronize
            {
                return getrandom.Next(min, max);
            }
        }
        //List_Add Method Implementation
        private static List<int> AddList(List<int> x, List<int> y)
        {
            List<int> Lista3 = new List<int>();
            int length1 = x.Count;
            int length2 = y.Count;
            if (length1 > length2) //x = 8 y =4
            {
                for(var i=0;i<length2;i++)
                {
                    Lista3.Add(x[i] + y[i]);
                }
                for (var z = length2; z < length1; z++)
                   Lista3.Add(x[z]);
            }
            return Lista3;  
        }
        //List_Zip methon implementation
        private static void PrintTosScreen<T>(List<T> x)
        {
            if (x.Count > 0)
            {
                for (int i = 0; i < x.Count-1; i++)
                {
                    Console.Write(x[i]);
                }
            }
              
        }
        private static List<object> Zip(List<object> leftList, List<object> rightList)
        {
            var result = new List<object>();
            int length;
            if (leftList.Count < rightList.Count)
            {
                length = leftList.Count;
            }
            else
                length = rightList.Count;
            for (int i = 0; i < length; i++)
            {
                result.Add(leftList[i]);
                result.Add(rightList[i]);
            }
            return result;
        }
    }
}